# -*- encoding: utf-8 -*-
=begin
= 仲間に関する設定

==更新履歴
Date     Version Author Comment
  :actor_id => 仲間になるアクターID
  :denominator => 仲間になる確率分母(1/N)
  :question => 質問セリフ,
  :yes => 承諾セリフ,
  :no => 拒否セリフ,

従来の設定では:denominatorに-1を設定すると
判定無視で仲間になる仕様でしたが使っていないようなので廃止にしました
=end

#==============================================================================
# ■ NWConst::Follower
#==============================================================================
module NWConst::Follower  
  SETTINGS = {
    3 => { # バニースライム娘
      :actor_id => 54, :denominator => 1,
      :question => ["【バニースライム娘】\nうさうさー！\nねぇ、あたしも連れてってよー！", "bunnyslime_fc1", 0],
      :yes => ["【バニースライム娘】\nあれ……キミは、あの裏山の時の……\nまあいっか、よろしくねー♪", "bunnyslime_fc1", 1],
      :no => ["【バニースライム娘】\nわーん、いじわるー！", "bunnyslime_fc1", 3],
    },
    4 => { # ナメクジ娘
      :actor_id => 55, :denominator => 2,
      :question => ["【ナメクジ娘】\n……気に入ったわ。\n私も、連れて行ってもらえないかしら……？", "name_fc1", 2],
      :yes => ["【ナメクジ娘】\nヌルヌルした手だけど、握手してくれる？\n……よろしくね。", "name_fc1", 3],
      :no => ["【ナメクジ娘】\n……ヌルヌルだからなのね。", "name_fc1", 1],
    },
    5 => { # インプ
      :actor_id => 56, :denominator => 2,
      :question => ["【インプ】\nキミについていったら、イイ事がありそう♪\nねぇ、あたしも仲間にしてよー♪", "inp_fc1", 0],
      :yes => ["【インプ】\nあははっ、よろしくねー♪\nはぐれちゃったおともだち、探してくれると嬉しいな♪", "inp_fc1", 0],
      :no => ["【インプ】\nいじわる……", "inp_fc1", 3],
    },
    6 => { # マンドラゴラ娘
      :actor_id => 59, :denominator => 4,
      :question => ["【マンドラゴラ娘】\nねぇねぇ、あたしも仲間にしてくれない？\n……断ったら、叫ぶからね。", "mdg_fc1", 0],
      :yes => ["【マンドラゴラ娘】\n土に埋まりながら、どこまでも付いていくわ。", "mdg_fc1", 0],
      :no => ["【マンドラゴラ娘】\nい　や　ぁ　ぁ　ぁ　ぁ　ぁ　ぁ　ぁ　ぁ…………", "mdg_fc1", 2],
    },
    7 => { # 犬娘
      :actor_id => 60, :denominator => 4,
      :question => ["【犬娘】\nひろって、わんわん……", "dog_fc1", 0],
      :yes => ["【犬娘】\nどこまでも、ついていくね……", "dog_fc1", 1],
      :no => ["【犬娘】\nくーん、くーん……", "dog_fc1", 2],
    },
    8 => { # ミミズ娘
      :actor_id => 61, :denominator => 4,
      :question => ["【ミミズ娘】\nねぇ、私も連れて行ってくれない？", "mimizu_fc1", 0],
      :yes => ["【ミミズ娘】\nふふっ、ミミズだって強い事を証明してあげるわ。", "mimizu_fc1", 1],
      :no => ["【ミミズ娘】\nこのまま干涸らびてたら、あんたのせいだから……", "mimizu_fc1", 2],
    },
    13 => { # ネズミ娘
      :actor_id => 70, :denominator => 4,
      :question => ["【ネズミ娘】\nあんたについていったら、美味しいモノ食べられる……？", "nezumi_fc1", 1],
      :yes => ["【ネズミ娘】\nやったぁ、いっぱい働くからね！", "nezumi_fc1", 1],
      :no => ["【ネズミ娘】\nいいもん……\nどうせあたしはネズミだし……", "nezumi_fc1", 0],
    },
    14 => { # 狼娘
      :actor_id => 71, :denominator => 2,
      :question => ["【狼娘】\nうがー！　あたしを連れてけ！\nあたしの牙と爪は役に立つぞ！", "okami_fc1", 0],
      :yes => ["【狼娘】\nアオーン……！\nオマエの敵、あたしが引き裂いてやる！", "okami_fc1", 3],
      :no => ["【狼娘】\nグルルルル……\nオマエ、けちんぼ。", "okami_fc1", 2],
    },
    16 => { # ヒル娘
      :actor_id => 73, :denominator => 4,
      :question => ["【ヒル娘】\nいろんなモンスターの体液を吸いたいわ。\nねぇ、私も冒険に連れて行ってくれる……？", "hiru_fc1", 0],
      :yes => ["【ヒル娘】\nふふっ、よろしくね。", "hiru_fc1", 1],
      :no => ["【ヒル娘】\nこの辺の男の体液、搾りまくってやるわ……", "hiru_fc1", 2],
    },
    17 => { # ウサギ娘
      :actor_id => 74, :denominator => 1,
      :question => ["【ウサギ娘】\nあなた、とても強いのね。\nねぇ、お姉さんも仲間にしてくれない？", "usagi_fc1", 0],
      :yes => ["【ウサギ娘】\nそれじゃあ、今日からあなたの仲間よ。\nふふっ、よろしくね。", "usagi_fc1", 3],
      :no => ["【ウサギ娘】\nあら、仲間は間に合ってるみたいね……", "usagi_fc1", 2],
    },
    18 => { # 羊娘
      :actor_id => 75, :denominator => 2,
      :question => ["【羊娘】\nういー、連れてってくれるのら？", "hituzi_fc1", 0],
      :yes => ["【羊娘】\nウェヒヒ、一緒に飲むのらー♪", "hituzi_fc1", 3],
      :no => ["【羊娘】\n独りで飲むのらー！", "hituzi_fc1", 2],
    },
    19 => { # シュリー
      :actor_id => 76, :denominator => 8,
      :question => ["【シュリー】\n……あたしも、外を冒険したーい！\nねー、連れてってよー！", "syuree_fc1", 0],
      :yes => ["【シュリー】\nわーい、外の世界を見られるんだー！", "syuree_fc1", 1],
      :no => ["【シュリー】\nここ、飽きたよぉ……\n外に出たいよぉ……", "syuree_fc1", 3],
    },
    20 => { # ジェイド
      :actor_id => 77, :denominator => 8,
      :question => ["【ジェイド】\n……あなた方との同行を希望します。", "jeid_fc1", 0],
      :yes => ["【ジェイド】\n同行の許可を得ました、これより外部に進出します……", "jeid_fc1", 0],
      :no => ["【ジェイド】\n同行の許可を得られませんでした。\n従来通り、巡回を続行……", "jeid_fc1", 2],
    },
    21 => { # シェスタ
      :actor_id => 78, :denominator => 8,
      :question => ["【シェスタ】\n思い出せない、私の記憶……\n探したい……私が何なのか……", "syesta_fc1", 0],
      :yes => ["【シェスタ】\n教えて、私が何なのか……", "syesta_fc1", 1],
      :no => ["【シェスタ】\n分からない……私は……", "syesta_fc1", 2],
    },
    23 => { # オーク娘
      :actor_id => 80, :denominator => 2,
      :question => ["【オーク娘】\nぶひっ、美味しいものを食べさせてくれます？", "orc_fc1", 0],
      :yes => ["【オーク娘】\nオークはあなたの仲間になりますよ！\n衣食住が保障されますね、やった！", "orc_fc1", 1],
      :no => ["【オーク娘】\nぶひ……", "orc_fc1", 2],
    },
    24 => { # ミツバチ娘
      :actor_id => 81, :denominator => 2,
      :question => ["【ミツバチ娘】\n私の蜜、これからの旅でも味わいたくない？\n特別に、ついていってあげてもいいわよ。", "mitubati_fc1", 0],
      :yes => ["【ミツバチ娘】\nふふっ……甘い蜜をたっぷり味わいなさい。", "mitubati_fc1", 1],
      :no => ["【ミツバチ娘】\n私の蜜は味わいたくないの？\nそう……残念ね。", "mitubati_fc1", 2],
    },
    25 => { # スズメ娘
      :actor_id => 82, :denominator => 2,
      :question => ["【スズメ娘】\nあ、あの……私も仲間にしてくれませんか？", "suzume_fc1", 0],
      :yes => ["【スズメ娘】\nほ、本当ですか……？　嬉しいです！", "suzume_fc1", 1],
      :no => ["【スズメ娘】\nそ、そうですよね……\nスズメなんか、戦力になりませんよね……", "suzume_fc1", 2],
    },
    26 => { # ハーピー
      :actor_id => 83, :denominator => 2,
      :question => ["【ハーピー】\nねぇねぇ、あたしも冒険に連れて行ってよ。", "hapy_a_fc1", 0],
      :yes => ["【ハーピー】\nあはっ、キミと一緒だったらどこまでも行くよ！", "hapy_a_fc1", 1],
      :no => ["【ハーピー】\nいいもんね……独りでだって飛べるんだから。", "hapy_a_fc1", 2],
    },
    27 => { # ハーピーツインズ
      :actor_id => 84, :denominator => 4,
      :question => ["【ハーピーツインズ】\nねぇ、ぴぃちゃんが一緒に旅したいって言ってるけど……\nもちろん、保護者の私も一緒にね。", "hapy_bc_fc1", 0],
      :yes => ["【ハーピーツインズ】\nよかったね、ぴぃちゃん。", "hapy_bc_fc1", 1],
      :no => ["【ハーピーツインズ】\nダメだって、ぴぃちゃん……", "hapy_bc_fc1", 2],
    },
    29 => { # ハイスラッグ娘
      :actor_id => 85, :denominator => 4,
      :question => ["【ハイスラッグ娘】\nふふっ、私をエスコートしてくれない？", "highslag_fc1", 0],
      :yes => ["【ハイスラッグ娘】\nふふふっ、良い子ね……\nご褒美に、ヌルヌルにしてあげてもいいわよ。", "highslag_fc1", 1],
      :no => ["【ハイスラッグ娘】\nなんて失礼な子なのかしら……", "highslag_fc1", 2],
    },
    30 => { # ナメクジシスター
      :actor_id => 86, :denominator => 8,
      :question => ["【ナメクジシスター】\n粘液まみれのシスターですが、同行しても良いですか？\n世界中にイリアス様の教えを広げたいのです。", "name_s_fc1", 0],
      :yes => ["【ナメクジシスター】\nそれでは、今後はあなたに同行しましょう。\nよろしくお願いしますね、ふふふっ。", "name_s_fc1", 1],
      :no => ["【ナメクジシスター】\nそうですか、残念です……\n粘液まみれのシスターなど、お呼びではないようですね。", "name_s_fc1", 2],
    },
    32 => { # ジャックオーランタン
      :actor_id => 88, :denominator => 2,
      :question => ["【ジャックオーランタン】\nあたしも冒険して、一人前になりたい……\nあのね、仲間にしてほしいな……", "jack_fc1", 0],
      :yes => ["【ジャックオーランタン】\nよろしくね、どこでもついてくから……", "jack_fc1", 1],
      :no => ["【ジャックオーランタン】\nご、ごめんなさい……", "jack_fc1", 2],
    },
    33 => { # ローパー娘
      :actor_id => 89, :denominator => 8,
      :question => ["【ローパー娘】\nお腹が減ったわね……\n食べ物のある所に連れてってくれる……？", "ropa_fc1", 0],
      :yes => ["【ローパー娘】\nいっぱい食べさせてね、ふふふっ……", "ropa_fc1", 1],
      :no => ["【ローパー娘】\nお腹が減ったわ……", "ropa_fc1", 2],
    },
    34 => { # メーダ娘
      :actor_id => 90, :denominator => 8,
      :question => ["【メーダ娘】\nかさかさ、こそこそこそ……", "meda_fc1", 0],
      :yes => ["【メーダ娘】\nこそこそ……よろしく……", "meda_fc1", 0],
      :no => ["【メーダ娘】\nもそもそもそ……", "meda_fc1", 0],
    },
    35 => { # キャンドル娘
      :actor_id => 91, :denominator => 8,
      :question => ["【キャンドル娘】\n命のある内に、広い世界を見てみたいわ……\n私の願い、叶えてくれない……？", "candle_fc1", 0],
      :yes => ["【キャンドル娘】\nふふっ、ありがとう……\nまあ私はアンデッドだから、死なないんだけどね。", "candle_fc1", 1],
      :no => ["【キャンドル娘】\n最期の願いも聞いてくれないなんて……", "candle_fc1", 2],
    },
    36 => { # イーター
      :actor_id => 92, :denominator => 8,
      :question => ["【イーター】\nねぇ、お外の世界につれていって！", "eater_fc1", 0],
      :yes => ["【イーター】\nわーい、いっぱい食べるぞー！", "eater_fc1", 3],
      :no => ["【イーター】\nここはやだよ、外に出たいよ……", "eater_fc1", 2],
    },
    37 => { # ライオット
      :actor_id => 93, :denominator => 8,
      :question => ["【ライオット】\n私を連れて行け……\nここでは暴れ足りん……", "liot_fc1", 0],
      :yes => ["【ライオット】\n外の世界には、どんな強者がいるのか……", "liot_fc1", 4],
      :no => ["【ライオット】\nもう、この世界の何もかもが飽きた……", "liot_fc1", 2],
    },
    38 => { # ルクスル
      :actor_id => 94, :denominator => 8,
      :question => ["【ルクスル】\nあはっ、私も連れて行きなさいよ。\n敵はみんな吸い尽くしてあげるから……", "luksl_fc1", 0],
      :yes => ["【ルクスル】\nあははっ、楽しみねぇ！\nあはははははっ！", "luksl_fc1", 1],
      :no => ["【ルクスル】\nあははははっ！　面白いわねぇ！", "luksl_fc1", 1],
    },
    39 => { # ダークエルフ
      :actor_id => 95, :denominator => 4,
      :question => ["【ダークエルフ】\nあなたと一緒に居ると、退屈しなさそうね。\n私の剣、役に立つと思わない？", "delh_a_fc1", 1],
      :yes => ["【ダークエルフ】\nふふっ、これで決まりね。\nこれからは私の剣を頼りにするといいわ。", "delh_a_fc1", 1],
      :no => ["【ダークエルフ】\n私の剣は必要ないってこと……！？", "delh_a_fc1", 2],
    },
    40 => { # ダークエルフ
      :actor_id => 96, :denominator => 8,
      :question => ["【ダークエルフ】\nこの子達は、もっとたくさんの養分を求めているわ。\nあなたについて行けば、たっぷり搾り取れそう……", "delh_b_fc1", 0],
      :yes => ["【ダークエルフ】\nふふっ、これからよろしくね。\nあなたの敵は、みんな私が搾ってあげるわ……", "delh_b_fc1", 1],
      :no => ["【ダークエルフ】\nそうなの、それは残念ね……", "delh_b_fc1", 2],
    },
    41 => { # フェアリー
      :actor_id => 97, :denominator => 2,
      :question => ["【フェアリー】\nねぇねぇ、あたしも冒険に行きたいなぁ♪", "fairy_fc1", 0],
      :yes => ["【フェアリー】\nわーい、よろしくね♪", "fairy_fc1", 0],
      :no => ["【フェアリー】\nイジワル……", "fairy_fc1", 2],
    },
    42 => { # ミニカニ娘
      :actor_id => 98, :denominator => 8,
      :question => ["【ミニカニ娘】\nわーい、あたしも一緒に冒険するー♪", "minikani_fc1", 0],
      :yes => ["【ミニカニ娘】\nあははっ、よろしくねー！", "minikani_fc1", 0],
      :no => ["【ミニカニ娘】\nうー！　うぇぇぇん！", "minikani_fc1", 2],
    },
    43 => { # ナマズ娘
      :actor_id => 99, :denominator => 8,
      :question => ["【ナマズ娘】\nたまには旅をするのもいいかのう……\n坊主、儂も連れて行ってはくれんか？", "namazu_fc1", 0],
      :yes => ["【ナマズ娘】\nふふっ、年甲斐もなくワクワクするのう。", "namazu_fc1", 1],
      :no => ["【ナマズ娘】\n大きな地震があったら、儂の怒りと思うが良い……", "namazu_fc1", 2],
    },
    44 => { # ラフレシア娘
      :actor_id => 100, :denominator => 8,
      :question => ["【ラフレシア娘】\nたまには、密林以外の場所にも行きたいわ……\nねぇ、あなたが連れて行ってよ。", "rahure_fc1", 0],
      :yes => ["【ラフレシア娘】\n私、密林の外に出るのは初めてなの。\nワクワクするわね、ふふふっ……", "rahure_fc1", 1],
      :no => ["【ラフレシア娘】\nやっぱり……この匂いのせい？", "rahure_fc1", 2],
    },
    45 => { # セントール
      :actor_id => 101, :denominator => 2,
      :question => ["【セントール】\nねぇ、あたしも旅に連れて行ってよ。\n走りには自信があるからさぁ……", "cent2_fc1", 0],
      :yes => ["【セントール】\nふふっ、今日からキミはあたしの仲間ね。\nボヤボヤしてると、先に行っちゃうから。", "cent2_fc1", 1],
      :no => ["【セントール】\nヘコんでなんて、いないんだから！", "cent2_fc1", 2],
    },
    46 => { # 銀狐二尾
      :actor_id => 102, :denominator => 2,
      :question => ["【銀狐二尾】\nねぇねぇ、あたしも仲間にしてよー！\n狐忍法、見せてあげるから！", "youko_fc1", 0],
      :yes => ["【銀狐二尾】\nわーい、これで君の仲間だね。\nむふふ～がんばるよ～！", "youko_fc1", 1],
      :no => ["【銀狐二尾】\nぎゃふん！", "youko_fc1", 2],
    },
    47 => { # かむろ二尾
      :actor_id => 103, :denominator => 2,
      :question => ["【かむろ二尾】\nあの、私を伴にしてはくれませんか……？\n若輩ですが、がんばりますので……", "kamuro_fc1", 0],
      :yes => ["【かむろ二尾】\nあ、ありがとうございます……！\n狐の秘術で、精一杯がんばりますね！", "kamuro_fc1", 1],
      :no => ["【かむろ二尾】\nすっ、すみません……", "kamuro_fc1", 2],
    },
    48 => { # クモ娘
      :actor_id => 104, :denominator => 16,
      :question => ["【クモ娘】\nふふっ、あたしを仲間にしてみない？\n邪魔な奴は、みんな餌食にしてあげるから。", "kumo_fc1", 0],
      :yes => ["【クモ娘】\nあはっ、やったぁ♪\nこれから食べ放題よね♪", "kumo_fc1", 1],
      :no => ["【クモ娘】\nなによ……\n他の人間がギセイになったら、あんたのせいなんだから！", "kumo_fc1", 2],
    },
    49 => { # ミミック娘
      :actor_id => 105, :denominator => 16,
      :question => ["【ミミック娘】\n引きこもるのも飽きたわ……\nねぇ、私も旅に連れて行ってくれない？", "mimic_fc1", 0],
      :yes => ["【ミミック娘】\n外の世界は、初めてよね……\nいい、私をしっかり守るのよ？", "mimic_fc1", 0],
      :no => ["【ミミック娘】\nいいわよ、ずっと引きこもってるから……", "mimic_fc1", 1],
    },
    52 => { # ナマコ娘
      :actor_id => 107, :denominator => 8,
      :question => ["【ナマコ娘】\nナマコだって、時には世界を旅したいわ……\nねぇ、私も冒険に連れて行ってもらえない？", "namako_fc1", 0],
      :yes => ["【ナマコ娘】\n世界中の色んなモノ、ナマコの体で味わってみるわ……\nふふっ、楽しみね。", "namako_fc1", 1],
      :no => ["【ナマコ娘】\nいいわよ……腹いせにそこらの男を襲うから。", "namako_fc1", 2],
    },
    53 => { # 貝娘
      :actor_id => 108, :denominator => 8,
      :question => ["【貝娘】\n旦那様、私と結婚して下さいますか？", "kai_fc1", 0],
      :yes => ["【貝娘】\n嬉しいです……\nどこまでもついていきますね、旦那様♪", "kai_fc1", 1],
      :no => ["【貝娘】\nいつかきっと、旦那様を振り向かせてみせますから……", "kai_fc1", 2],
    },
    54 => { # カニ娘
      :actor_id => 109, :denominator => 4,
      :question => ["【カニ娘】\nねぇ、あたしも連れて行ってよ。\n汚れた時は、泡でたっぷりサービスしてあげるから♪", "kani_fc1", 0],
      :yes => ["【カニ娘】\nあははっ、これからよろしく。\n体が汚れた時は、私に言ってね……♪", "kani_fc1", 1],
      :no => ["【カニ娘】\nふんだ、あんたなんか大嫌いよ！", "kani_fc1", 2],
    },
    55 => { # スライムベス娘
      :actor_id => 110, :denominator => 2,
      :question => ["【スライムベス娘】\nキミについていくと、いろいろ食べられるかなぁ？", "slimelord_fc1", 4],
      :yes => ["【スライムベス娘】\nわーい、なんでも溶かして食べちゃうぞー♪", "slimelord_fc1", 1],
      :no => ["【スライムベス娘】\n食べたいよぉ……", "slimelord_fc1", 2],
    },
    56 => { # スイカ娘
      :actor_id => 111, :denominator => 8,
      :question => ["【スイカ娘】\nねぇ、あたしも冒険に連れて行ってほしいなぁ……", "suika_fc1", 0],
      :yes => ["【スイカ娘】\nわーい、やったぁ！\nスイカ割りとか、やめてよね。", "suika_fc1", 1],
      :no => ["【スイカ娘】\n……スイカは嫌い？", "suika_fc1", 2],
    },
    57 => { # ウツボカズラ娘
      :actor_id => 112, :denominator => 8,
      :question => ["【ウツボカズラ娘】\nあんたについて行くと、色んな獲物が食べられそう。\nねぇ、私も連れて行きなさいよ。", "utubo_fc1", 0],
      :yes => ["【ウツボカズラ娘】\n世界中の虫を食べ歩くの、楽しみね。\nもちろん、もっと大きな生き物も……", "utubo_fc1", 1],
      :no => ["【ウツボカズラ娘】\nいつか溶かしてやるわ……", "utubo_fc1", 3],
    },
    58 => { # ラミア
      :actor_id => 113, :denominator => 4,
      :question => ["【ラミア】\n私も、世界を旅してみようかしら。\nねぇ、私も連れて行ってくれるわよね？", "lamia_fc1", 0],
      :yes => ["【ラミア】\nちゃんと養ってくれるんでしょうね？\n……それじゃ、よろしくね。", "lamia_fc1", 1],
      :no => ["【ラミア】\n失礼なのね、あなた。\nこの私の同行を断るなんて……", "lamia_fc1", 2],
    },
    59 => { # ワカメ娘
      :actor_id => 114, :denominator => 8,
      :question => ["【ワカメ娘】\nゆらゆら……私も行きたい……", "wakame_fc1", 0],
      :yes => ["【ワカメ娘】\nわかめだけど、よろしくね……", "wakame_fc1", 1],
      :no => ["【ワカメ娘】\nゆらゆらゆら……", "wakame_fc1", 2],
    },
    60 => { # クラゲ娘
      :actor_id => 115, :denominator => 8,
      :question => ["【クラゲ娘】\n私も……冒険、したい……", "kurage_fc1", 0],
      :yes => ["【クラゲ娘】\n世界を回る、楽しそう……", "kurage_fc1", 1],
      :no => ["【クラゲ娘】\nがっかり……", "kurage_fc1", 0],
    },
    61 => { # イソギンチャク娘
      :actor_id => 116, :denominator => 8,
      :question => ["【イソギンチャク娘】\nふふっ、あなた気に入ったわ。\nこの私が、仲間になってあげる", "iso_fc1", 0],
      :yes => ["【イソギンチャク娘】\n海から出るのは初めてね……ふふっ、楽しみだわ。", "iso_fc1", 1],
      :no => ["【イソギンチャク娘】\n私の力は必要ないっていうの？\n屈辱だわ……", "iso_fc1", 2],
    },
    62 => { # アンコウ娘
      :actor_id => 117, :denominator => 8,
      :question => ["【アンコウ娘】\n……………………", "ankou_fc1", 0],
      :yes => ["【アンコウ娘】\nよろ……しく……", "ankou_fc1", 0],
      :no => ["【アンコウ娘】\n……………………", "ankou_fc1", 0],
    },
    64 => { # 17ページ
      :actor_id => 119, :denominator => 4,
      :question => ["【17ページ】\n我も同行しよう……", "page17_fc1", 0],
      :yes => ["【17ページ】\n今後ともよろしく……", "page17_fc1", 0],
      :no => ["【17ページ】\n無念……", "page17_fc1", 0],
    },
    65 => { # 257ページ
      :actor_id => 120, :denominator => 8,
      :question => ["【257ページ】\nあら、なんて素敵な方。\n私も同行してよろしいでしょうか？", "page257_fc1", 0],
      :yes => ["【257ページ】\nふふっ……よろしくお願いしますね。", "page257_fc1", 1],
      :no => ["【257ページ】\n私の力は必要ありませんか……残念です。", "page257_fc1", 2],
    },
    67 => { # シスターラミア
      :actor_id => 122, :denominator => 8,
      :question => ["【シスターラミア】\nあなたは、正しい人だとお見受けしました。\n供に世界を巡ること、許して下さいませんか？", "sisterlamia_fc1", 0],
      :yes => ["【シスターラミア】\n寛容な心に感謝致します。\nさあ、世界に神の教えを広げるために参りましょう。", "sisterlamia_fc1", 4],
      :no => ["【シスターラミア】\n私の見込み違いだったようですね。\n神よ、良き供に巡り合わせ下さい……", "sisterlamia_fc1", 3],
    },
    68 => { # シスキュバス
      :actor_id => 123, :denominator => 8,
      :question => ["【シスキュバス】\n偶然にも、私も世界を旅する身なのです。\nここは、共に行きませんか……？", "sisccubus_fc1", 0],
      :yes => ["【シスキュバス】\nなんと心強いことでしょう。\n私の力、存分にお使い下さい。", "sisccubus_fc1", 1],
      :no => ["【シスキュバス】\n道は交わりませんでしたか……残念です。", "sisccubus_fc1", 2],
    },
    69 => { # アリクイ娘
      :actor_id => 124, :denominator => 8,
      :question => ["【アリクイ娘】\n戦いは苦手だけど、私も連れて行ってくれない？\n伸びる舌も、きっと役に立つはずだから……", "arikui_fc1", 0],
      :yes => ["【アリクイ娘】\nいっぱいがんばるから、見捨てないで……", "arikui_fc1", 1],
      :no => ["【アリクイ娘】\nどうせ、私は役立たずだから……", "arikui_fc1", 2],
    },
    70 => { # グリズリー娘
      :actor_id => 125, :denominator => 4,
      :question => ["【グリズリー娘】\nうががー！　オマエと一緒に行きたいぞ！", "grizzly_fc1", 0],
      :yes => ["【グリズリー娘】\nうががー！　まかせろー！", "grizzly_fc1", 1],
      :no => ["【グリズリー娘】\nうが……オマエ、いじわる……", "grizzly_fc1", 2],
    },
    71 => { # XX-7
      :actor_id => 126, :denominator => 8,
      :question => ["【XX-7】\nこの私をお供にしてくれる……？", "XX-7_fc1", 0],
      :yes => ["【XX-7】\nそれじゃあ、従うわ……", "XX-7_fc1", 0],
      :no => ["【XX-7】\n残念ね……", "XX-7_fc1", 0],
    },
    72 => { # リトルバグ
      :actor_id => 127, :denominator => 8,
      :question => ["【リトルバグ】\nオイ、アタシも連れていけ！", "littlebug_fc1", 0],
      :yes => ["【リトルバグ】\nやった、これで外に出られるな！", "littlebug_fc1", 0],
      :no => ["【リトルバグ】\nアタシ、ずっとこのままなのか……", "littlebug_fc1", 2],
    },
    73 => { # キャンサーロイド
      :actor_id => 128, :denominator => 16,
      :question => ["【キャンサーロイド】\n人間、私を外に連れて行け。\nそれなりの働きは約束しよう……", "kaniloid_fc1", 0],
      :yes => ["【キャンサーロイド】\nそれでいい……\n私の実力、証明してやろう", "kaniloid_fc1", 1],
      :no => ["【キャンサーロイド】\n狭量だな……", "kaniloid_fc1", 2],
    },
    75 => { # ゴースト
      :actor_id => 130, :denominator => 4,
      :question => ["【ゴースト】\n私も……連れて行って……", "ghost_fc1", 0],
      :yes => ["【ゴースト】\n幽霊だけど、よろしく……", "ghost_fc1", 1],
      :no => ["【ゴースト】\nああ、恨めしい……", "ghost_fc1", 0],
    },
    76 => { # 呪いの人形娘
      :actor_id => 131, :denominator => 4,
      :question => ["【呪いの人形娘】\nおにいちゃん……もっと一緒にあそびたい……", "doll_fc1", 0],
      :yes => ["【呪いの人形娘】\nうふふっ、ずっと一緒だよ……", "doll_fc1", 1],
      :no => ["【呪いの人形娘】\nあそびたい……あそびたい……", "doll_fc1", 2],
    },
    77 => { # ゾンビ剣士
      :actor_id => 132, :denominator => 4,
      :question => ["【ゾンビ剣士】\n私も……行きたい……", "zonbe_fc2", 0],
      :yes => ["【ゾンビ剣士】\nこの剣……あなたのために……", "zonbe_fc2", 0],
      :no => ["【ゾンビ剣士】\nこの剣、ここで朽ち果てるのみ……", "zonbe_fc2", 0],
    },
    78 => { # ゾンビ娘
      :actor_id => 133, :denominator => 8,
      :question => ["【ゾンビ娘】\nあぅぅ……\n連れて……行って……", "zonbe_fc3", 0],
      :yes => ["【ゾンビ娘】\n一緒に……行く……", "zonbe_fc3", 0],
      :no => ["【ゾンビ娘】\n……………………", "zonbe_fc3", 2],
    },
    84 => { # エルフ
      :actor_id => 136, :denominator => 4,
      :question => ["【エルフ】\nあなた、何だか頼りないわね。\nせっかくだし、私が一緒に行ってあげてもいいけど？", "elf_fc1", 0],
      :yes => ["【エルフ】\n仕方ないわねぇ……\nそれじゃあ、私の弓で守ってあげるわ。", "elf_fc1", 1],
      :no => ["【エルフ】\nふん……どうにでもなればいいわ。", "elf_fc1", 2],
    },
    85 => { # ブラウニーズ
      :actor_id => 137, :denominator => 8,
      :question => ["【ブラウニーズ】\nねぇ、お兄ちゃん。\nあたし達のお友達になってよ♪", "braunys_fc1", 6],
      :yes => ["【ブラウニーズ】\nわーい、いっしょに遊ぼうねー♪", "braunys_fc1", 7],
      :no => ["【ブラウニーズ】\nお友達になりたかったな……", "braunys_fc2", 0],
    },
    86 => { # ツインズフェアリー
      :actor_id => 138, :denominator => 8,
      :question => ["【ツインズフェアリー】\nねぇ、あたし達も連れて行ってほしいな。\n妖精の力で、守ってあげる！", "tfairy_fc1", 0],
      :yes => ["【ツインズフェアリー】\nあたし達だって、役に立つよ。\n妖精の力、見せてあげるんだから！", "tfairy_fc1", 1],
      :no => ["【ツインズフェアリー】\n妖精だって……役に立つんだからぁ……", "tfairy_fc1", 2],
    },
    87 => { # フェアリーズ
      :actor_id => 139, :denominator => 8,
      :question => ["【フェアリーズ】\nねぇねぇ、あたし達とおともだちになってよ♪", "fairys_fc1", 0],
      :yes => ["【フェアリーズ】\nわーい、みんなー！\nこの人が、あそんでくれるんだって！", "fairys_fc1", 0],
      :no => ["【フェアリーズ】\nいじわるー！　いじわるー！", "fairys_fc1", 2],
    },
    95 => { # タランチュラ娘
      :actor_id => 140, :denominator => 8,
      :question => ["【タランチュラ娘】\nねぇ、私も一緒に行っていいかしら？\n戦闘には自信があるわ……", "taran_fc1", 0],
      :yes => ["【タランチュラ娘】\nこれからよろしく。\nせいぜい頑張るとするわ。", "taran_fc1", 1],
      :no => ["【タランチュラ娘】\n下半身が蜘蛛の女は嫌い……？", "taran_fc1", 2],
    },
    96 => { # ミノタウロス娘
      :actor_id => 141, :denominator => 4,
      :question => ["【ミノタウロス娘】\nお前、見かけによらず強いんだな。\nなぁ、あたしも連れてってくれよ！", "mino_fc1", 1],
      :yes => ["【ミノタウロス娘】\nいっぱい暴れるから、メシはよろしくな！", "mino_fc1", 0],
      :no => ["【ミノタウロス娘】\nおいおい、ふざけんなー！？", "mino_fc1", 2],
    },
    97 => { # バンダースナッチ娘
      :actor_id => 142, :denominator => 8,
      :question => ["【バンダースナッチ娘】\nお前は、強いな……\n私も一緒に行こう……", "bander_fc1", 0],
      :yes => ["【バンダースナッチ娘】\nお前の敵は、全て私が倒してやろう……", "bander_fc1", 0],
      :no => ["【バンダースナッチ娘】\nそうか……残念だ……", "bander_fc1", 2],
    },
    99 => { # サボレス
      :actor_id => 144, :denominator => 4,
      :question => ["【サボレス】\nねぇ、アタシも仲間になってあげようか？", "saboresu_fc1", 0],
      :yes => ["【サボレス】\nあははっ、よろしくね～！", "saboresu_fc1", 1],
      :no => ["【サボレス】\nなによー！　ひどーい！", "saboresu_fc1", 2],
    },
    100 => { # ムカデ娘
      :actor_id => 145, :denominator => 8,
      :question => ["【ムカデ娘】\nあなたの冒険に、私を連れて行く気はない……？", "mukade_fc1", 0],
      :yes => ["【ムカデ娘】\nムカデだって、ただの嫌われ者じゃない事を見せてあげるわ。", "mukade_fc1", 1],
      :no => ["【ムカデ娘】\n嫌われ者だものね、ムカデ……", "mukade_fc1", 2],
    },
    101 => { # サソリ娘
      :actor_id => 146, :denominator => 4,
      :question => ["【サソリ娘】\n砂漠も飽きたわ、私を連れ出してくれない？", "sasori_fc1", 0],
      :yes => ["【サソリ娘】\n砂漠以外の場所でも、私は結構強いわよ。\nふふっ、よろしくね。", "sasori_fc1", 1],
      :no => ["【サソリ娘】\nいいわよ、ずっと砂漠で生きていくから……", "sasori_fc1", 2],
    },
    102 => { # サボテン娘
      :actor_id => 147, :denominator => 8,
      :question => ["【サボテン娘】\nうふふっ、あなた素敵ですわ……\nこの私を、側に置いて下さいませんか？", "saboten_fc1", 0],
      :yes => ["【サボテン娘】\nあなたの敵に思い知らせてやりましょう。\n綺麗なサボテンには、トゲがある事を……", "saboten_fc1", 1],
      :no => ["【サボテン娘】\nなんという屈辱……\nいつか、トゲだらけにしてやりますわ。", "saboten_fc1", 2],
    },
    103 => { # ダチョウ娘
      :actor_id => 148, :denominator => 4,
      :question => ["【ダチョウ娘】\nねぇねぇ、あたしも連れてって下さいよー！\n空は飛べないけど、走るのは速いですよ！", "datyou_fc1", 0],
      :yes => ["【ダチョウ娘】\nわーい、一生懸命がんばりますねー！", "datyou_fc1", 3],
      :no => ["【ダチョウ娘】\nやっぱり、空が飛べないとダメなんですかぁ……？", "datyou_fc1", 2],
    },
    104 => { # ランプの魔女
      :actor_id => 149, :denominator => 8,
      :question => ["【ランプの魔女】\n私から願いを言っても良いでしょうか……？\nあなたの従者にしてもらいたいのですけれど。", "lamp_fc1", 0],
      :yes => ["【ランプの魔女】\nふふっ、それでは常にあなたの側に……\n私の力、あなたのために使いましょう。", "lamp_fc1", 1],
      :no => ["【ランプの魔女】\n私の力は望まないというのですね……？\nそれでは、大人しく去るとしましょう。", "lamp_fc1", 2],
    },
    105 => { # ミイラ娘
      :actor_id => 150, :denominator => 4,
      :question => ["【ミイラ娘】\n勇者よ、我をピラミッドから解き放つが良い……", "mummy_fc1", 0],
      :yes => ["【ミイラ娘】\n我も共に行こう、勇者よ……", "mummy_fc1", 1],
      :no => ["【ミイラ娘】\n我が力、必要としない……か。", "mummy_fc1", 2],
    },
    106 => { # コブラ娘
      :actor_id => 151, :denominator => 8,
      :question => ["【コブラ娘】\nあなた、強いわね……\n私も連れて行ってくれないかしら？", "kobura_fc1", 0],
      :yes => ["【コブラ娘】\n砂漠の外には、どんな世界が広がっているのかしら……\nふふっ、楽しみだわ。", "kobura_fc1", 1],
      :no => ["【コブラ娘】\n私には、乾いた場所がお似合いというコトかしら……？", "kobura_fc1", 2],
    },
    107 => { # ネフェルラミアス
      :actor_id => 152, :denominator => 8,
      :question => ["【ネフェルラミアス】\nあなた、とっても素敵よね……\n私達姉妹の面倒も見てくれない？", "lamias_fc2", 4],
      :yes => ["【ネフェルラミアス】\n私達四人、あなたの力になるわ。\nだから、あなたも……分かってるわよねぇ？", "lamias_fc2", 4],
      :no => ["【ネフェルラミアス】\nふん、甲斐性のない男よねぇ……", "lamias_fc2", 4],
    },
    111 => { # ワニ娘
      :actor_id => 154, :denominator => 8,
      :question => ["【ワニ娘】\nねぇ、あたしも仲間になっていい……？", "wani_fc1", 0],
      :yes => ["【ワニ娘】\nそれじゃあ、よろしくね……", "wani_fc1", 1],
      :no => ["【ワニ娘】\n残念……それじゃ、またの機会にね。", "wani_fc1", 2],
    },
    113 => { # デビルファイター
      :actor_id => 156, :denominator => 4,
      :question => ["【デビルファイター】\nあたしも冒険に連れて行ってくれないかな……？\n恥ずかしい格好だけど、腕は保証するから……ね？", "d_fighter_fc1", 0],
      :yes => ["【デビルファイター】\nそれじゃあ、今日からよろしくね。\n出来れば、この格好も着替えたいんだけど……", "d_fighter_fc1", 1],
      :no => ["【デビルファイター】\nそ、そう……それじゃあ、さよなら。", "d_fighter_fc1", 0],
    },
    114 => { # 大蜘蛛
      :actor_id => 157, :denominator => 8,
      :question => ["【大蜘蛛】\nおい、あたしがついて行ってやろうか？", "oogumo_fc1", 0],
      :yes => ["【大蜘蛛】\n大船に乗ったつもりでいろ！\nあたし、結構強いんだからな。", "oogumo_fc1", 1],
      :no => ["【大蜘蛛】\nふん……勝手にしろよ！", "oogumo_fc1", 2],
    },
    115 => { # サックボア
      :actor_id => 158, :denominator => 8,
      :question => ["【サックボア】\n……………………", "suckvore_fc1", 2],
      :yes => ["【サックボア】\n……………………", "suckvore_fc1", 2],
      :no => ["【サックボア】\n……………………", "suckvore_fc1", 2],
    },
    116 => { # アイアンメイデン
      :actor_id => 159, :denominator => 16,
      :question => ["【アイアンメイデン】\n……………………", "ironmaiden_fc1", 0],
      :yes => ["【アイアンメイデン】\n……………………", "ironmaiden_fc1", 1],
      :no => ["【アイアンメイデン】\n……………………", "ironmaiden_fc1", 0],
    },
    117 => { # ワームビレッジャ
      :actor_id => 160, :denominator => 8,
      :question => ["【ワームビレッジャ】\n……もっと吸いたいわ。私も連れて行って……", "wormv_fc1", 0],
      :yes => ["【ワームビレッジャ】\nもっと吸いたいの……\nもっともっと……", "wormv_fc1", 1],
      :no => ["【ワームビレッジャ】\n残念ね、吸いたかったのに……", "wormv_fc1", 2],
    },
    118 => { # ウーストレル
      :actor_id => 161, :denominator => 8,
      :question => ["【ウーストレル】\nうふふっ、私も連れて行ってくれませんか？\n世界を旅するのも、とっても面白そう……", "ustrel_fc1", 0],
      :yes => ["【ウーストレル】\n楽しみですね、ふふふっ……", "ustrel_fc1", 1],
      :no => ["【ウーストレル】\nそう……それは仕方ないですね。", "ustrel_fc1", 2],
    },
    119 => { # 蜜壺
      :actor_id => 162, :denominator => 16,
      :question => ["【蜜壺】\nねぇ、あたしも冒険に連れて行ってよ。\nもちろん、壺から出る気はないけどね……", "mitutubo_fc1", 0],
      :yes => ["【蜜壺】\n壺に入ったまま世界を回る……\nそういうのも、楽しいよね。", "mitutubo_fc1", 1],
      :no => ["【蜜壺】\nあんたなんか、嫌い……", "mitutubo_fc1", 2],
    },
    121 => { # 小鬼
      :actor_id => 164, :denominator => 4,
      :question => ["【小鬼】\nねぇ、あたしも付いていっていい？\nあたしの力、とっても頼りになるよ！", "kooni_fc1", 0],
      :yes => ["【小鬼】\nやったぁ、よろしくね！\n悪者はみんな、あたしの棍棒で叩き潰してあげる！", "kooni_fc1", 1],
      :no => ["【小鬼】\nいいもん、あたし一人でも旅くらいできるんだから！", "kooni_fc1", 3],
    },
    122 => { # オナホ娘
      :actor_id => 165, :denominator => 8,
      :question => ["【オナホ娘】\n私も、ついていっていいですかぁ？\n溜まった時には、役に立っちゃいますから♪", "onaho_fc1", 0],
      :yes => ["【オナホ娘】\nあはっ、よろしくお願いしますー♪\n溜まった時は、いつでも言って下さいね♪", "onaho_fc1", 0],
      :no => ["【オナホ娘】\nえっちな器具は側に置かない主義なんですか……？", "onaho_fc1", 2],
    },
    123 => { # ナーキュバス
      :actor_id => 166, :denominator => 8,
      :question => ["【ナーキュバス】\nもう、困った患者さんですね……\nあたしがつきっきりで、看病してあげますよ？", "narcubus_fc1", 0],
      :yes => ["【ナーキュバス】\nそれじゃあ、よろしくお願いしますー。\n何かあったら、私を頼って下さいね♪", "narcubus_fc1", 3],
      :no => ["【ナーキュバス】\nもう、看護婦の言うことは聞かなきゃダメですよ……", "narcubus_fc1", 2],
    },
    125 => { # 百々目鬼
      :actor_id => 168, :denominator => 8,
      :question => ["【百々目鬼】\nあなた、なかなか面白いわね……\n私の力も貸してあげるわ。", "dodome_fc1", 0],
      :yes => ["【百々目鬼】\nそれじゃあ、手伝ってあげる。\nこの瞳の魔力、好きに使うといいわ……", "dodome_fc1", 1],
      :no => ["【百々目鬼】\nふん！　悔しくないから！", "dodome_fc1", 2],
    },
    126 => { # リザードシーフ
      :actor_id => 169, :denominator => 4,
      :question => ["【リザードシーフ】\nお前、とっても気に入ったぜ。\nなあ、お前んとこの団にあたしも入れてくれないか？", "lizardthief_fc1", 0],
      :yes => ["【リザードシーフ】\nよっしゃ、今日からあたしもお前んとこの一員だ！\nこれから、よろしくな！", "lizardthief_fc1", 0],
      :no => ["【リザードシーフ】\nあたしじゃ、力不足ってわけか……？\nじゃあ、腕を上げてまた来るぜ！", "lizardthief_fc1", 2],
    },
    127 => { # リザードシーフ
      :actor_id => 170, :denominator => 8,
      :question => ["【リザードシーフ】\nあなた、かなりの手練れのようですね……\n私も同行させてもらえませんか？", "lizardthief_fc2", 0],
      :yes => ["【リザードシーフ】\nふふっ……これからよろしく。\n剣と盗みなら、私に任せてもらいましょう。", "lizardthief_fc2", 0],
      :no => ["【リザードシーフ】\nそう、それは残念ですね……", "lizardthief_fc2", 2],
    },
    129 => { # 蜃気楼娘
      :actor_id => 172, :denominator => 8,
      :question => ["【蜃気楼娘】\nねぇ、私も連れて行ってもらえませんか？", "sinkiro_fc1", 0],
      :yes => ["【蜃気楼娘】\nわーい、がんばりますよー！", "sinkiro_fc1", 4],
      :no => ["【蜃気楼娘】\nうぇーん、ひどーい！", "sinkiro_fc1", 3],
    },
    130 => { # アリジゴク娘
      :actor_id => 173, :denominator => 8,
      :question => ["【アリジゴク娘】\nあなたといると、獲物がたくさん集まりそうね。\n釣り餌として最適かも……", "arizigoku_fc1", 0],
      :yes => ["【アリジゴク娘】\nふふっ……よりどりみどり搾り放題ね。\nもちろんあなたは、最後にしてあげるから……", "arizigoku_fc1", 1],
      :no => ["【アリジゴク娘】\nそう……うまい話はないものね。", "arizigoku_fc1", 2],
    },
    131 => { # サンドワーム娘
      :actor_id => 174, :denominator => 16,
      :question => ["【サンドワーム娘】\n一緒に……行きたい……", "sandw_fc1", 0],
      :yes => ["【サンドワーム娘】\nよろしく……", "sandw_fc1", 1],
      :no => ["【サンドワーム娘】\n大きいから……？", "sandw_fc1", 2],
    },
    132 => { # デザートスキュラ
      :actor_id => 175, :denominator => 8,
      :question => ["【デザートスキュラ】\nあははっ、あたしもついて行ってあげようかー？", "d_scylla_fc1", 0],
      :yes => ["【デザートスキュラ】\nそれじゃあ、よろしくねー♪", "d_scylla_fc1", 1],
      :no => ["【デザートスキュラ】\nなんだ、つまんないのー", "d_scylla_fc1", 2],
    },
    134 => { # ヴィタエ
      :actor_id => 176, :denominator => 16,
      :question => ["【ヴィタエ】\nこの場所には飽きました。\n私を、外の世界へ招待して下さいませんか……？", "vitae_fc1", 0],
      :yes => ["【ヴィタエ】\nとても楽しみですね。\n外の世界で、何が待っているのでしょう……", "vitae_fc1", 1],
      :no => ["【ヴィタエ】\nあら、なんと残念なことでしょう……", "vitae_fc1", 2],
    },
    135 => { # ヴェータラ
      :actor_id => 177, :denominator => 16,
      :question => ["【ヴェータラ】\n喜びなさい、ボウヤ……\nお姉さんが仲間になってあげるわ。", "vetala_fc1", 0],
      :yes => ["【ヴェータラ】\nこれでしばらく、空腹は免れそうねぇ……\nうふふっ、ボウヤも食べられてみたい……？", "vetala_fc1", 1],
      :no => ["【ヴェータラ】\nそう、そんなに食べてほしいのかしら……？", "vetala_fc1", 2],
    },
    136 => { # ヴァルト
      :actor_id => 178, :denominator => 16,
      :question => ["【ヴァルト】\n同伴の許可を願う……", "valt_fc1", 0],
      :yes => ["【ヴァルト】\n了解、以降はマスターに従う……", "valt_fc1", 0],
      :no => ["【ヴァルト】\n拒絶……", "valt_fc1", 2],
    },
    137 => { # シニファ
      :actor_id => 179, :denominator => 16,
      :question => ["【シニファ】\nアッハハハハハハ！", "sinifa_fc1", 0],
      :yes => ["【シニファ】\nヒャーハハハハハハ！", "sinifa_fc1", 1],
      :no => ["【シニファ】\nアハハ……", "sinifa_fc1", 2],
    },
    138 => { # シャドー娘
      :actor_id => 180, :denominator => 8,
      :question => ["【シャドー娘】\n私を……連れて行け……", "shadow_fc1", 0],
      :yes => ["【シャドー娘】\nお前に憑いて行く……", "shadow_fc1", 0],
      :no => ["【シャドー娘】\n残念……無念……", "shadow_fc1", 0],
    },
    139 => { # ガイストビーネ
      :actor_id => 181, :denominator => 8,
      :question => ["【ガイストビーネ】\nこの世界は、絵の中よりも広いのかしら……\nねぇ、私を連れ出してくれない……？", "gaistvine_fc1", 0],
      :yes => ["【ガイストビーネ】\nそれじゃあ、冒険に行きましょう……", "gaistvine_fc1", 0],
      :no => ["【ガイストビーネ】\nふん……どうせ、絵の中の方が素敵よ。", "gaistvine_fc1", 1],
    },
    140 => { # キメラホムンクルス
      :actor_id => 182, :denominator => 8,
      :question => ["【キメラホムンクルス】\n私の存在意義を知りたい……\nお前についていけば、知ることが出来るのか……？", "c_homunculus_fc1", 0],
      :yes => ["【キメラホムンクルス】\nお前についていこう……", "c_homunculus_fc1", 1],
      :no => ["【キメラホムンクルス】\nお前では……駄目か……", "c_homunculus_fc1", 2],
    },
    141 => { # アイアンメイデン改
      :actor_id => 183, :denominator => 16,
      :question => ["【アイアンメイデン改】\n罪人を搾るため……私も行く……", "ironmaiden_k_fc1", 0],
      :yes => ["【アイアンメイデン改】\nお前の敵……罪人……\n全て、搾り尽くす……", "ironmaiden_k_fc1", 1],
      :no => ["【アイアンメイデン改】\nお前も……やはり罪人……", "ironmaiden_k_fc1", 2],
    },
    142 => { # ジャンクドール娘
      :actor_id => 184, :denominator => 16,
      :question => ["【ジャンクドール娘】\nあたし達も、連れて行ってくれる……？\n大丈夫、悪いようにはしないわ……", "junkdoll_fc2", 0],
      :yes => ["【ジャンクドール娘】\nありがとう……ふふふふっ。\nあなたの敵、全てジャンクにしてあげるわ……", "junkdoll_fc2", 0],
      :no => ["【ジャンクドール娘】\nジャンクだから、不要とでも……？\n", "junkdoll_fc2", 0],
    },
  }
end



